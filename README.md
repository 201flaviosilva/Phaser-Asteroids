# Phaser Asteroids

## Description
A Phaser Asteroids game
## Links
- [GitLab](https://gitlab.com/201flaviosilva/Phaser-Asteroids);
  - [Issue](https://gitlab.com/201flaviosilva/Phaser-Asteroids/issues);
- [Play](https://201flaviosilva.gitlab.io/Phaser-Asteroids/);

## URL Parameters

| Parameter | Description                   | Example                               | Options                                         | Required |
| --------- | ----------------------------- | ------------------------------------- | ----------------------------------------------- | -------- |
| `debug`   | Show Phaser debug information | `http://localhost:9999/?debug=true`   | `true`/`false`                                  | false    |
| `mode`    | How is playing                | `http://localhost:9999/?mode=AIRadar` | `Player`/`AIClosest`/`AIRadar`/`AIHillClimbing` | false    |

### Complete URL Example
- Dev: http://localhost:9999/?debug=true&mode=AIClosest
- Production: https://201flaviosilva.gitlab.io/Phaser-Asteroids/?debug=false&mode=Player

## Available Commands

| Command           | Description                                                                     |
| ----------------- | ------------------------------------------------------------------------------- |
| `npm run clear`   | Delete the "build" and "out" and "dist" folder                                  |
| `npm install`     | Install project dependencies                                                    |
| `npm start`       | Start project and open web server running project                               |
| `npm run build`   | Builds code bundle with production settings (minification, uglification, etc..) |
| `npm run preview` | Builds code bundle with development settings (no minification, no uglification) |
| `npm run bump`    | Bump version and deploy a new version                                           |
