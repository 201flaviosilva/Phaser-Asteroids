import Phaser from "phaser";

import "../node_modules/awesome-notifications/dist/style.css";
import "./style.css";

import Config from "./Config";

import Game from "./Scenes/Game";
import Preload from "./Scenes/Preload";

const config = {
  width: Config.width,
  height: Config.height,
  type: Phaser.AUTO,
  backgroundColor: "#000000",
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  transparent: false,
  antialias: true,
  physics: {
    default: "arcade",
    arcade: {
      debug: Config.debug,
    },
  },
  parent: "app",
  scene: [Preload, Game],
};

window.addEventListener("load", () => new Phaser.Game(config));
