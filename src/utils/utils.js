export function getUrlParameter(key) {
	const urlSearchParams = new URLSearchParams(window.location.search);
	const params = Object.fromEntries(urlSearchParams.entries());
	return params[key];
}
export function isDebugMode() { return getUrlParameter("debug") === "true"; }
