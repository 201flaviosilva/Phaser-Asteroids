import HillClimbing from "hillclimbing";

import Config from "../Config";

export class HillClimbingUtils {
	constructor() {
		if (HillClimbingUtils.instance instanceof HillClimbingUtils)
			return HillClimbingUtils.instance;

		const targets = [
			// Player
			{
				name: "playerForwardVelocity",
				value: Config.player.speed,
				min: 1,
				max: 500
			},
			{
				name: "playerRotationSpeed",
				value: Config.player.rotationSpeed,
				min: 1,
				max: 500
			},
			{
				name: "playerForward",
				value: Number(Config.player.forward),
				min: 0,
				max: 1
			},
			{
				name: "playerLeft",
				value: Number(Config.player.left),
				min: 0,
				max: 1
			},
			{
				name: "playerRight",
				value: Number(Config.player.right),
				min: 0,
				max: 1
			},
			// Asteroids
			{
				name: "asteroidsDestroyDivider",
				value: Config.asteroids.destroyDivider,
				min: 1,
				max: 5
			},
			{
				name: "asteroidsStartAstroides",
				value: Config.asteroids.startAstroides,
				min: 1,
				max: 20
			},
			{
				name: "asteroidsStartLives",
				value: Config.asteroids.startLives,
				min: 1,
				max: 3
			},
			{
				name: "asteroidsMaxSpeed",
				value: Config.asteroids.maxSpeed,
				min: 1,
				max: 500
			},
			// Bullets
			{
				name: "bulletShootMarginTime",
				value: Config.bullet.shootMarginTime,
				min: 50,
				max: 1000
			},
			{
				name: "bulletSpeed",
				value: Config.bullet.speed,
				min: 1,
				max: 500
			}
		];
		this.hcState = new HillClimbing(targets);

		HillClimbingUtils.instance = this;
	}

	run(score) {
		console.log("-----------");
		const newSolution = this.hcState.run(score);

		console.log(this.hcState.getLastTargetChanged().name);
		console.log(newSolution);

		// Player
		Config.player.speed = this.hcState.getCurrentTargetValueSolutionByName(
			"playerForwardVelocity"
		);
		Config.player.rotationSpeed = this.hcState.getCurrentTargetValueSolutionByName(
			"playerRotationSpeed"
		);
		Config.player.playerForward = !!this.hcState.getCurrentTargetValueSolutionByName(
			"playerForward"
		);
		Config.player.playerLeft = !!this.hcState.getCurrentTargetValueSolutionByName(
			"playerLeft"
		);
		Config.player.playerRight = !!this.hcState.getCurrentTargetValueSolutionByName(
			"playerRight"
		);

		// Asteroids
		Config.asteroids.destroyDivider = this.hcState.getCurrentTargetValueSolutionByName(
			"asteroidsDestroyDivider"
		);
		Config.asteroids.startAstroides = this.hcState.getCurrentTargetValueSolutionByName(
			"asteroidsStartAstroides"
		);
		Config.asteroids.startLives = this.hcState.getCurrentTargetValueSolutionByName(
			"asteroidsStartLives"
		);
		Config.asteroids.maxSpeed = this.hcState.getCurrentTargetValueSolutionByName(
			"asteroidsMaxSpeed"
		);

		// Bullets
		Config.bullet.shootMarginTime = this.hcState.getCurrentTargetValueSolutionByName(
			"bulletShootMarginTime"
		);
		Config.bullet.speed = this.hcState.getCurrentTargetValueSolutionByName(
			"bulletSpeed"
		);
	}
}

export default new HillClimbingUtils();
