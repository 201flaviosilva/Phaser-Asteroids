import Player from "../Assets/Player.png";
import Asteroid from "../Assets/Asteroid.png";

export default class Preload extends Phaser.Scene {
	constructor() {
		super({ key: "Preload" });
	}

	preload() {
		this.load.image("Player", Player);
		this.load.image("Asteroid", Asteroid);

		this.load.on("complete", () => { this.scene.start("Game"); });
	}
}
