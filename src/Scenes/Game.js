import AWN from "awesome-notifications";

import Config from "../Config";

import { getUrlParameter } from "../utils/utils";
import HillClimbingUtils from "../utils/HillClimbingUtils";

import PlayerGroup from "../Prefabs/Player";
import BulletGroup from "../Prefabs/Bullet";
import AsteroidGroup from "../Prefabs/Asteroid";

const PLAYER_MODES = {
	player: "Player",
	aiClosest: "AIClosest",
	aiRadar: "AIRadar",
	aiHillClimbing: "AIHillClimbing",
};

export default class Game extends Phaser.Scene {
	constructor() {
		super("Game");

		this.notifier = new AWN();
	}

	init(options) {
		if (options.gameOver) this.notifier.warning("Game Over");
		else if (options.winGame) this.notifier.success("You Win!");
		this.nextShootTime = 0;
		this.score = 0;

		this.playerMode = getUrlParameter("mode") || PLAYER_MODES.player;
	}

	create() {
		// Player
		const playerGroup = new PlayerGroup(this.physics.world, this);
		this.player = playerGroup.getNewPlayer();
		this.bulletGroup = new BulletGroup(this.physics.world, this);

		// Asteroids
		this.asteroidGroup = new AsteroidGroup(this.physics.world, this);
		for (let i = 0; i < Config.asteroids.startAstroides; i++) {
			this.asteroidGroup.getNewAsteroid();
		}

		// Score
		this.scoreText = this.add.text(16, 16, "Score: 0", { fontSize: "32px", fill: "#fff" });

		// Collisions
		// Bullet vs Asteroid
		this.physics.add.overlap(this.bulletGroup, this.asteroidGroup, (bullet, asteroid) => {
			if (asteroid.lives > 1) {
				this.asteroidGroup.getNewAsteroid(asteroid.x, asteroid.y, asteroid.lives - 1);
				this.asteroidGroup.getNewAsteroid(asteroid.x, asteroid.y, asteroid.lives - 1);
			}
			this.setScore(this.score + asteroid.lives * 10);
			asteroid.destroy();
			bullet.destroy();

			// Win
			if (this.asteroidGroup.countActive(true) === 0) {
				if (this.playerMode === PLAYER_MODES.aiHillClimbing) HillClimbingUtils.run(this.score + 100);
				this.scene.restart({ winGame: true });
			}
		}, null, this);

		// Player vs Asteroid -> Game Over
		this.physics.add.overlap(this.player, this.asteroidGroup, () => {
			if (this.playerMode === PLAYER_MODES.aiHillClimbing) HillClimbingUtils.run(this.score - 100);
			this.scene.restart({ gameOver: true });
		}, null, this);

		// Player Input
		if (this.playerMode === PLAYER_MODES.player) this.cursors = this.input.keyboard.createCursorKeys();

		if (Config.debug) this.debugGraphics = this.add.graphics();

		this.events.on("render", this.debug, this);
	}

	setScore(score) {
		this.score = score;
		this.scoreText.setText(`Score: ${score}`);
	}

	shoot(time) {
		if (this.nextShootTime < time) {
			this.bulletGroup.getNewBullet(this.player.x, this.player.y, this.player.angle);
			this.nextShootTime = time + Config.bullet.shootMarginTime;
		}
	}

	update(time, delta) {
		if (this.playerMode === PLAYER_MODES.player) this.playerControlling(time);
		else if (this.playerMode === PLAYER_MODES.aiClosest) this.aiClosestControlling(time);
		else if (this.playerMode === PLAYER_MODES.aiRadar) this.aiRadarControlling(time);
		else if (this.playerMode === PLAYER_MODES.aiHillClimbing) this.aiHillClimbing(time);

		// Update bullets
		this.bulletGroup.children.each(bullet => bullet.update(time));

		// Update asteroids
		this.asteroidGroup.children.each(asteroid => asteroid.update());
	}

	debug(render) {
		if (!this.debugGraphics) return;

		const { x, y } = this.player;
		// Clear
		this.debugGraphics.clear();

		if (!this.asteroidGroup.countActive(true)) return;

		{ // Closest and Furthest Asteroid
			const closestAsteroid = this.physics.closest(this.player, this.asteroidGroup.getChildren());
			const furthestAsteroid = this.physics.furthest(this.player, this.asteroidGroup.getChildren());
			this.debugGraphics.lineStyle(2, 0xffff00, 1)
				.lineBetween(closestAsteroid.x, closestAsteroid.y, x, y)
				.lineStyle(2, 0xff00ff, 1)
				.lineBetween(furthestAsteroid.x, furthestAsteroid.y, x, y);
		}

		{ // Radar
			const { numberSlices, radius } = Config.AIClosest;
			const sliceAngle = Phaser.Math.RadToDeg(Math.PI * 2 / numberSlices);

			this.debugGraphics.lineStyle(1, 0xffffff, 1)
				.fillStyle(0xffffff, 0.1);

			for (let i = 0; i < numberSlices; i++) {
				const startAngle = i * sliceAngle - 180;
				const endAngle = startAngle + sliceAngle;

				this.debugGraphics.beginPath();
				this.debugGraphics.slice(x, y, radius, Phaser.Math.DegToRad(startAngle), Phaser.Math.DegToRad(endAngle));

				this.asteroidGroup.getChildren().forEach(asteroid => {
					const distance = Phaser.Math.Distance.BetweenPoints(this.player, asteroid);
					const asteroidAngle = Phaser.Math.RadToDeg(Phaser.Math.Angle.Between(x, y, asteroid.x, asteroid.y));

					if (distance < radius && (asteroidAngle > startAngle && asteroidAngle < endAngle)) this.debugGraphics.fillPath();
				});
				this.debugGraphics.strokePath();
			}
		}
	}

	playerControlling(time) {
		// Move player
		this.player.update({
			forward: this.cursors.up.isDown,
			left: this.cursors.left.isDown,
			right: this.cursors.right.isDown,
		});

		// Shoot
		if (this.cursors.space.isDown) this.shoot(time);
	}

	aiClosestControlling(time) {
		if (!this.asteroidGroup.countActive(true)) return;
		// get closest asteroid
		const closestAsteroid = this.physics.closest(this.player, this.asteroidGroup.getChildren());

		const angleToPointer = Phaser.Math.Angle.Between(this.player.x, this.player.y, closestAsteroid.x, closestAsteroid.y);
		const angleDelta = Phaser.Math.Angle.Wrap(angleToPointer - this.player.rotation);

		this.player.update({
			forward: false,
			left: angleDelta < 0,
			right: angleDelta > 0,
		});

		// TODO: Try to predict the position of the closest asteroid, based in the asteroids velocity: https://gitlab.com/201flaviosilva/Phaser-Asteroids/-/issues/1
		if (Math.abs(angleDelta) < 1) this.shoot(time);
	}

	aiRadarControlling(time) {
		if (!this.asteroidGroup.countActive(true)) return;

		const { numberSlices, radius } = Config.AIClosest;
		const angleBySlice = Phaser.Math.RadToDeg(Math.PI * 2 / numberSlices);

		// Calculate the number of asteroids in each slice
		let slices = [];
		for (let i = 0; i < numberSlices; i++) {
			const startAngle = i * angleBySlice - 180;
			const endAngle = startAngle + angleBySlice;
			const middleAngle = startAngle + angleBySlice / 2;
			const slice = { middleAngle, numberOfAsteroids: 0, };

			this.asteroidGroup.getChildren().forEach(asteroid => {
				const distance = Phaser.Math.Distance.BetweenPoints(this.player, asteroid);
				const asteroidAngle = Phaser.Math.RadToDeg(Phaser.Math.Angle.Between(this.player.x, this.player.y, asteroid.x, asteroid.y));

				if (distance < radius && (asteroidAngle > startAngle && asteroidAngle < endAngle)) slice.numberOfAsteroids++;
			});

			slices.push(slice);
		}

		// Find the slice with the most asteroids
		const sliceWithMostAsteroids = slices.reduce((sliceA, sliceB) => sliceA.numberOfAsteroids > sliceB.numberOfAsteroids ? sliceA : sliceB);

		if (sliceWithMostAsteroids.numberOfAsteroids <= 0) {
			this.player.update({ forward: false, left: false, right: false, });
			return;
		}
		const fixPlayerAngle = sliceWithMostAsteroids.middleAngle - this.player.angle;

		this.player.update({
			forward: false,
			left: fixPlayerAngle < 0,
			right: fixPlayerAngle > 0,
		});

		if (Math.abs(fixPlayerAngle) < angleBySlice / 2) this.shoot(time);
	}

	aiHillClimbing(time) {
		// Move player
		this.player.update({
			forward: Config.player.forward,
			left: Config.player.left,
			right: Config.player.right
		});

		// Shoot
		if (true) this.shoot(time);
	}
}
