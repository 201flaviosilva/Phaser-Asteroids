import Phaser from "phaser";

import Config from "../Config";

export class Player extends Phaser.Physics.Arcade.Sprite {
	constructor(scene, x, y) {
		super(scene, x, y, "Player");
		scene.physics.add.existing(this);

		this.speed = Config.player.speed;
		this.rotationSpeed = Config.player.rotationSpeed;

		this.setDamping(true);
		this.setDrag(0.9);
		this.setMaxVelocity(this.speed);
	}

	update({ forward, left, right }) {
		if (forward) this.scene.physics.velocityFromRotation(this.rotation, this.speed, this.body.acceleration);
		else this.setAcceleration(0);

		if (left) this.setAngularVelocity(-this.rotationSpeed);
		else if (right) this.setAngularVelocity(this.rotationSpeed);
		else this.setAngularVelocity(0);

		// Reposition the player if they go off screen
		if (this.x >= Config.width) this.x = 0;
		else if (this.x <= 0) this.x = Config.width;

		if (this.y >= Config.height) this.y = 0;
		else if (this.y <= 0) this.y = Config.height;
	}
}

export default class PlayersGroup extends Phaser.Physics.Arcade.Group {
	constructor(world, scene) {
		const config = {
			classType: Player,
			runChildUpdate: true,
		};
		super(world, scene, config);
	}

	getNewPlayer(x = 640, y = 360) {
		const player = this.get(x, y, true);
		return player;
	}
}
