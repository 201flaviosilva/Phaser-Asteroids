import Config from "../Config";

export class Asteroid extends Phaser.Physics.Arcade.Sprite {
	constructor(scene, x, y) {
		super(scene, x, y, "Asteroid");
		scene.physics.add.existing(this);

		this.lives = Config.asteroids.startLives;
		this.maxSpeed = Config.asteroids.maxSpeed;
		this.speed = Phaser.Math.Between(5, this.maxSpeed);

		this.setCircle(this.width / 2);
		this.setAngle(Phaser.Math.Between(0, 360));
		this.setMaxVelocity(this.maxSpeed);
	}

	setLives(lives) {
		this.lives = lives;
		this.setScale(lives * 0.40);
	}

	update() {
		this.scene.physics.velocityFromRotation(this.rotation, this.speed, this.body.velocity);

		if (this.x > Config.width) this.x = 0;
		else if (this.x < 0) this.x = Config.width;

		if (this.y > Config.height) this.y = 0;
		else if (this.y < 0) this.y = Config.height;
	}
}

export default class AsteroidGroup extends Phaser.Physics.Arcade.Group {
	constructor(world, scene) {
		const config = {
			classType: Asteroid,
			runChildUpdate: true,
		};
		super(world, scene, config);
	}

	getNewAsteroid(x = 0, y = 0, newLives = Config.asteroids.startLives) {
		const asteroid = this.get(x, y);
		if (newLives === Config.asteroids.startLives) asteroid.setRandomPosition();
		else asteroid.setLives(newLives);
		return asteroid;
	}
}
