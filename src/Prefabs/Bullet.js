import Config from "../Config";
export class Bullet extends Phaser.Physics.Arcade.Sprite {
	constructor(scene, x, y) {
		super(scene, x, y, "Player");

		this.speed = Config.bullet.speed;
		this.time = 0;

		this.setScale(0.25);
	}

	update(time) {
		if (!this.tim) this.time = time + 150;

		this.scene.physics.velocityFromRotation(this.rotation, this.speed, this.body.velocity);

		if (time > this.time ||
			this.x > Config.width || this.x < 0
			|| this.y > Config.height || this.y < 0) this.destroy();
	}
}

export default class BulletGroup extends Phaser.Physics.Arcade.Group {
	constructor(world, scene) {
		const config = {
			classType: Bullet,
			runChildUpdate: true,
		};
		super(world, scene, config);
	}

	getNewBullet(x = 0, y = 0, angle = 0) {
		return this.get(x, y).setAngle(angle);
	}
}
