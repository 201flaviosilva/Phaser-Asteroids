import { isDebugMode } from "./utils/utils";

export class Config {
	constructor() {
		if (Config.instance instanceof Config) return Config.instance;

		this.width = 1280; // 16:9 // https://calculateaspectratio.com
		this.height = 720;
		this.debug = isDebugMode();
		this.player = {
			speed: 200,
			rotationSpeed: 300,
			forward: false,
			left: true,
			right: false,
		};
		this.asteroids = {
			destroyDivider: 2, // New "child" asteroid will be created after destroy
			startAstroides: 1,
			startLives: 3,
			maxSpeed: 100,
		};
		this.bullet = {
			shootMarginTime: 150,
			speed: 200,
		};
		this.AIClosest = {
			numberSlices: 16,
			radius: 500,
		};

		Config.instance = this;
	}
}

export default new Config();
